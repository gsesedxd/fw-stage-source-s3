#!/usr/bin/env node
import 'source-map-support/register';
import cdk = require('@aws-cdk/core');
import { FwStageSourceS3Stack } from '../lib/fw-stage-source-s3-stack';

const app = new cdk.App();
new FwStageSourceS3Stack(app, 'FwStageSourceS3Stack');
