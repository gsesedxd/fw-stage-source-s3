import cdk = require('@aws-cdk/core');
import s3 = require('@aws-cdk/aws-s3');

// @WIP: Importar de otra manera
import { Constants } from './../../FW-CoreLib/lib/constants';
import { Category, Owner, Provider } from './../../fw-corelib/lib/configuration';

export class FwStageSourceS3Stack extends cdk.Stack {

  private bucketS3 : s3.CfnBucket;

  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

      this.bucketS3 = new s3.CfnBucket(this, 'BucketS3', {
      bucketName: `${Constants.IDProject}-${Constants.EnvProject}-s3bucket`,
      accessControl: 'PublicRead',
      versioningConfiguration: {
        status: 'Enabled'
      }
    });

    // Outputs
    new cdk.CfnOutput(this, 'SourceS3', {
      value: this.bucketS3.bucketName ? this.bucketS3.bucketName : ''
    });
  }

  getUselessConfig(codepipeline_actions: any){
    return {
      stageName: 'Source',
      actions: [
        new codepipeline_actions.S3SourceAction({
          bucket: this.bucketS3.bucketName,
          bucketKey: cdk.Fn.join('',  [
            Constants.ProjectRepositoryName,
            "/",
            Constants.RepositoryName,
            `/${Constants.EnvProject === 'pro' ? 'master' : 'developer'}/branch-${Constants.EnvProject === 'pro' ? 'master' : 'developer'}/`,
            Constants.ProjectRepositoryName,
            "_",
            Constants.RepositoryName,
            "_",
            `${ Constants.EnvProject === 'pro' ? 'master' : 'developer'}_branch - ${ Constants.EnvProject === 'pro' ? 'master' : 'developer'}.zip`
        ])
      })
      ]
    }
  }

  getConfig() {
    return {
      name: 'Source',
        actions: [
          {
            name: 'S3',
            actionTypeId: {
              category: Category.APPROVAL,
              owner: Owner.AWS,
              provider: Provider.S3,
              version: '1'
            },
            // inputArtifacts: [
            //   {
            //     name: 'inputArtifact'
            //   }
            // ],
            // outputArtifacts: [
            //   {
            //     name: 'outputArtifact'
            //   }
            // ],
            runOrder: 1,
            configuration: {
              S3Bucket: this.bucketS3.bucketName,
              S3ObjectKey: cdk.Fn.join('', [
                Constants.ProjectRepositoryName,
                "/",
                Constants.RepositoryName,
                `/${Constants.EnvProject === 'pro' ? 'master' : 'developer'}/branch-${Constants.EnvProject === 'pro' ? 'master' : 'developer'}/`,
                Constants.ProjectRepositoryName,
                "_",
                Constants.RepositoryName,
                "_",
                `${Constants.EnvProject === 'pro' ? 'master' : 'developer'}_branch - ${Constants.EnvProject === 'pro' ? 'master' : 'developer'}.zip`
              ])
            }
          }
        ],
    }
  }

}
